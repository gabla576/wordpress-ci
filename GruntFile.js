/**
 * Too many issues with yeoman. Dumping it for a process that is a bit easier to maintain.
 */
module.exports = function (grunt) {

    var path = require('path');
    var env = grunt.option('env') ? grunt.option('env') : 'local';

    // Plugin loading - load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    // Load our custom grunt tasks
    grunt.loadTasks('grunt_tasks');

    // Load grunt task configurations automatically
    require('load-grunt-config')(grunt, {

        /**
         * Path to grunt task config files
         */
        configPath: path.join(process.cwd(), 'grunt_tasks/config'),

        // 'load-grunt-config' expects grunt config options inside the 'data' key
        data: {

            ENVIRONMENT: env,

            /**
             * Constants so we can easily change environment paths.
             */
            BASE_PATH: '.',

            /**
             * Location of WordPress source files
             */
            SOURCE_PATH: 'wp/',

            /**
             * Location of build folder where we generate the app files for different environments
             */
            BUILD_PATH: './',

            /**
             * Location of temporary files (transitional files that are pre-processed, minified or compiled) before being copied to the build path
             */
            TEMP_PATH: 'temp/',

            /**
             * Location of static files to include inside the app folder (images, fonts, audio, etc...)
             */
            RESOURCES_PATH: 'assets/',

            pkg: grunt.file.readJSON('package.json'),

            config: grunt.file.readJSON('grunt_tasks/env.' + env + '.json')

        }
    });


    grunt.registerTask('demo2', 'Test task2', function(isThisAVar2) {
            grunt.task.run('demo:'+isThisAVar2);
        }
    );

    grunt.registerTask('wp-install', 'Custom WordPress installation tasks', function(args) {
            //grunt.task.run('demo:'+args);
            grunt.task.run([
                'copy:wp-install',
                'replace:wp-install',
                'replace:wp-config'
            ]);
        }
    );
};
