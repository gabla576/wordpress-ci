module.exports = {
    "wp-install": {
        files: [
            {
                src: ['<%= SOURCE_PATH %>.htaccess'],
                dest: '<%= BUILD_PATH %>',
                filter: 'isFile',
                expand: true,
                flatten: true
            }
        ]
    }
};
