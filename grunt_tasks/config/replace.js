module.exports = {
    // http://codex.wordpress.org/Giving_WordPress_Its_Own_Directory
    "wp-install": {
        options: {
            patterns: [
                {
                    match: '/wp-blog-header.php',
                    replacement: '/<%= SOURCE_PATH %>wp-blog-header.php'
                }
            ],
            usePrefix: false
        },
        files: [
            {expand: true, flatten: true, src: ['<%= SOURCE_PATH %>index.php'], dest: '<%= BUILD_PATH %>'}
        ]
    },
    // depends on env.local.json containing environment configuration variables
    "wp-config": {
        options: {
            patterns: [
                {
                    match: "'database_name_here'",
                    replacement: "getenv('<%= config.wp.DB_NAME %>')"
                },
                {
                    match: "'username_here'",
                    replacement: "getenv('<%= config.wp.DB_USER %>')"
                },
                {
                    match: "'password_here'",
                    replacement: "getenv('<%= config.wp.DB_PASSWORD %>')"
                },
                {
                    match: "localhost",
                    replacement: '<%= config.wp.DB_HOST %>'
                }
            ],
            usePrefix: false
        },
        files: [
            {src: ['<%= SOURCE_PATH %>wp-config-sample.php'], dest: '<%= BUILD_PATH %>wp-config.php'}
        ]
    }
};